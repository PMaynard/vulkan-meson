Meson and C build of the Vulkan tutorial located at <https://vulkan-tutorial.com>  

# Dependencies

	apt install libvulkan-dev libglfw3-dev 
	dnf install vulkan-devel glfw-devel 

# Build

	meson build
	ninja -C build
	ninja -C build scan-build # static analysis.

# Troubleshooting 

Check that you can run ```vulkaninfo``` without an issue. From vulkan-tools pacakge.

# References 

- <https://vulkan-tutorial.com>
- <http://www.glfw.org/docs/latest/group__vulkan.html>
- <https://renderdoc.org/vulkan-in-30-minutes.html>
