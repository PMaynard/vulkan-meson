#include <stdio.h>
#include <stdlib.h>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include "main.h"

const int WIDTH = 800;
const int HEIGHT = 600;

GLFWwindow* window = NULL;
VkInstance instance;
VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

int initWindow(void)
{
    if (!glfwInit())
    {
        printf("GLFW: Initialisation Failed\n");
        return -1;
    }

    if (glfwVulkanSupported() == GLFW_FALSE)
    {
        printf("Vulkan not supported.\n");
        return -1;
    }

    glfwSetErrorCallback(error_callback);

    // Create a window.
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan window", NULL, NULL);
    if (!window)
    {
        printf("GLFW: Unable to create window\n");
        return -1;
    }
    return 0;
}

int mainLoop(void)
{
    while(!glfwWindowShouldClose(window)) {
        glfwPollEvents();
    }

    return 0;
}

int createInstance(void)
{
    // provide some useful information to the driver to optimize for our specific application
    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Hello Triangle";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    // not optional and tells the Vulkan driver which global extensions and validation layers we want to use.
    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;
    createInfo.enabledLayerCount = 0;

    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
    if(!glfwExtensions)
    {
        printf("GLFW: Unable to get required instance extensions. (%d)\n", glfwExtensionCount);
    }
    
    createInfo.enabledExtensionCount = glfwExtensionCount;
    createInfo.ppEnabledExtensionNames = glfwExtensions;

    VkResult result = vkCreateInstance(&createInfo, NULL, &instance);
    if (result != VK_SUCCESS)
    {
        printf("VK: Unable to create instance - ");
        switch(result) {
            case VK_ERROR_OUT_OF_HOST_MEMORY: 
                printf("VK_ERROR_OUT_OF_HOST_MEMORY\n");
                break;
            case VK_ERROR_OUT_OF_DEVICE_MEMORY:
                printf("VK_ERROR_OUT_OF_DEVICE_MEMORY\n");
                break;
            case VK_ERROR_INITIALIZATION_FAILED: 
                printf("VK_ERROR_INITIALIZATION_FAILED\n");
                break;
            case VK_ERROR_LAYER_NOT_PRESENT: 
                printf("VK_ERROR_LAYER_NOT_PRESENT\n");
                break;
            case VK_ERROR_EXTENSION_NOT_PRESENT:
                printf("VK_ERROR_EXTENSION_NOT_PRESENT\n");
                break;
            case VK_ERROR_INCOMPATIBLE_DRIVER:
                printf("VK_ERROR_INCOMPATIBLE_DRIVER\n");
                break;
            default:
                printf("createInstance(): Unknown VK_ERROR\n");
                break;
        }
        return -1;
    }
    return 0;
}

int pickPhysicalDevice(void)
{
    uint32_t deviceCount = 0;
    VkResult result = vkEnumeratePhysicalDevices(instance, &deviceCount, NULL);
    if (result != VK_SUCCESS)
    {
        printf("VK: Unable to find physical device - ");
        switch (result) {
            case VK_INCOMPLETE:
                printf("VK_INCOMPLETE\n");
                break;
            case VK_ERROR_OUT_OF_HOST_MEMORY:
                printf("VK_ERROR_OUT_OF_HOST_MEMORY\n");
                break;
            case VK_ERROR_OUT_OF_DEVICE_MEMORY:
                printf("VK_ERROR_OUT_OF_DEVICE_MEMORY\n");
                break;
            case VK_ERROR_INITIALIZATION_FAILED:
                printf("VK_ERROR_INITIALIZATION_FAILED\n");
                break;
            default:
                printf("pickPhysicalDevice(): Unknown VK_ERROR\n");
                break;
        }
        return -1;
    }

    if (deviceCount <= 0) {
        printf("Not able to find a physical device.\n");
        return -1;
    }

    return 0;
}

int initVulkan(void)
{
    if(createInstance())
        return -1;

    if(pickPhysicalDevice())
        return -1;

    return 0;
}

void cleanup(void)
{
    vkDestroyInstance(instance, NULL);
    glfwDestroyWindow(window);
    glfwTerminate();
}

int main() {

    printf("initWindow.\n");
    if(initWindow())
    {
        cleanup();
        return EXIT_FAILURE;
    }

    printf("initVulkan.\n");
    if(initVulkan())
    {
        cleanup();
        return EXIT_FAILURE;   
    }

    printf("mainLoop.\n");
    if(mainLoop())
    {
        cleanup();
        return EXIT_FAILURE;
    }

    printf("Final CleanUp.\n");
    cleanup();
    return EXIT_SUCCESS;
}

