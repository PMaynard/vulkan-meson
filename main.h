void error_callback(int error, const char* description);
int initWindow(void);
int mainLoop(void);
int createInstance(void);
int pickPhysicalDevice(void);
int initVulkan(void);
void cleanup(void);